#!/bin/sh

# валюта по умолчанию
VALUTE="usd"
# количество рублей по умолчанию
RUBS=1

help(){
    echo "sh $0 [COUNT] [CURRENCY_CODE]"
}

if [ "$1" = "-h" -o "$1" = "" ]
then
    help
    exit 0
fi

if [ "$1" != "" ]
then
    RUBS="$1"
fi

if [ "$2" != "" ]
then
    VALUTE="$2"
fi
# оставляем только первую указанную валюту
VALUTE=$(echo ${VALUTE} | sed "s/^\([^\,]*\).*/\1/")
# меняем запятые на точки
COST=$(python rub_currency.py -v$VALUTE | sed "s/\,/\./")
# считаем и выводим
echo "${COST}*${RUBS}" | bc | sed "s/\(.*\)/Rubs in ${RUBS} ${VALUTE}: \1/"
