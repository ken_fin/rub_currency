#!/usr/bin/python
# -*- coding: utf8 -*-
import getopt, re, datetime, sys
import CBRValuteParser

PROG_NAME = 'rub_currency'
PROG_DESC = 'Python script for get currency in RUB'

# настройки
valute = 'usd'
date = datetime.datetime.today().strftime('%d.%m.%Y')
no_cache = False
# регулярное выражение для парсинга даты
date_re_pattern = "^(\d{2})[\D]?(\d{2})[\D]?(\d{4})$"

def usage(exit_status = 0):
	print("Usage: %s.py [options]\n" % PROG_NAME)
	print("%s\n" % PROG_DESC)
	print("Options:\n")
	print("\t-v \t\t valute code (ex: USD, EUR)")
	print("\t-d \t\t currency by date (format: DD-MM-YYYY (DDMMYYYY))")
	print("\t-f \t\t dont use cache")
	print("\t--clear-cache \t clear currency cache")
	print("\t--help \t\t show this help\n")

	sys.exit(exit_status)

# инициализация парсера cbr.ru
parser = CBRValuteParser.CBRValuteParser()

try:
	opts, args = getopt.getopt(sys.argv[1:], "v:d:f", ["clear-cache", "help"])
except getopt.GetoptError:
	usage(1)

# обработка аргументов к.с.
for opt,arg in opts:
	# указаны валюты
	if opt == '-v':
		valute = arg
	# указана дата
	elif opt == '-d':
		if re.match(date_re_pattern, arg):
			date = re.sub(date_re_pattern, r"\1.\2.\3", arg)
		else:
			raise Exception("Invalid date format! Must be DD-MM-YYYY (DDMMYYYY)!")
	# ключ для работы без использования кэша
	elif opt == '-f':
		no_cache = True
	# ключ для очистки кэша 
	elif opt == '--clear-cache':
		parser.clear_cache()
	elif opt == '--help':
		usage(0)
	else:
		usage(2)

parser.download_valute_list(date, no_cache)

for val in valute.split(','):
	try:
		print(parser.get_valute(val.upper()))
	except IndexError:
		print("Error! Code <" + val + "> not found!")
		sys.exit(1)